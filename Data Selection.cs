﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CS_RFID3_Host_Sample2
{

    public partial class Data_Selection : Form
    {
        public static class Globals
        {
            public static string AntArrang;
            public static string Exp;
            public static Int16 Reps;
            public static Int16 count = 0;
            public static Int16 AntCount = 8;
            public static Int16 ItemNumber1;
            public static Int16 ItemNumber2;
            public static Int16 ItemNumber3;
        }
        public Data_Selection()
        {
            InitializeComponent();
        }

        private void Data_Selection_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 7;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.AntArrang = comboBox1.Text;
            Globals.Exp = comboBox2.Text;
            Globals.Reps = Convert.ToInt16(textBox1.Text);
            Globals.ItemNumber1 = Convert.ToInt16(textBox2.Text);
            Globals.ItemNumber2 = Convert.ToInt16(textBox3.Text);
            Globals.ItemNumber3 = Convert.ToInt16(textBox4.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //string Starting = GlobalVars.path + @"\Output.csv";
            //string Archive = GlobalVars.path + @"\Results\" + Globals.Exp + " - " + DateTime.Now.Month + "." + DateTime.Now.Day + " - " + DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second + ".csv";
            //System.IO.FileInfo file = new System.IO.FileInfo(GlobalVars.path + @"\Results\");
            //file.Directory.Create();
            //System.IO.File.Move(Starting, Archive);
            //MessageBox.Show("Successfully Archived");
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
